import React from 'react'

const Customers = React.lazy(() => import('./views/Shop24h/Customers'))
const Orders = React.lazy(() => import('./views/Shop24h/Orders'))
const Products = React.lazy(() => import('./views/Shop24h/Products'))
const ProductTypes = React.lazy(() => import('./views/Shop24h/ProductTypes'))
const Shop24hHome = React.lazy(() => import('./views/Shop24h/Shop24hHome'))

const routes = [
  { path: '/', name: '', element: Shop24hHome, exact: true },
  { path: '/Customers', name: 'Customers', element: Customers },
  { path: '/Orders', name: 'Orders', element: Orders },
  { path: '/Products', name: 'Products', element: Products },
  { path: '/ProductTypes', name: 'Product Types', element: ProductTypes },
]

export default routes
