import React from 'react'
import CIcon from '@coreui/icons-react'
import {
  cilCart,
  cilStorage
} from '@coreui/icons'
import { CNavGroup, CNavItem } from '@coreui/react'

const _nav = [
  {
    component: CNavItem,
    name: 'Shop24h',
    to: '/',
    icon: <CIcon icon={cilCart} customClassName="nav-icon" />,
  },
  {
    component: CNavGroup,
    name: 'Manager',
    to: '/',
    icon: <CIcon icon={cilStorage} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'Customers',
        to: '/Customers',
      },
      {
        component: CNavItem,
        name: 'Orders',
        to: '/Orders',
      },
      {
        component: CNavItem,
        name: 'Products',
        to: '/Products',
      },
      {
        component: CNavItem,
        name: 'Product Types',
        to: '/ProductTypes',
      },
    ],
  },
]

export default _nav
