import React, { useEffect, useState } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from '@coreui/react'

const Orders = () => {
  const [OrdersData, setOrdersData] = useState([{
    cost: 0,
    note: "",
    orderDate: "",
    shippedDate: ""
  }])

  const fetchApi = async (url) => {
    const response = await fetch(url);
    const data = await response.json();
    return data;
  }

  useEffect(() => {
    fetchApi("http://localhost:8000/Orders")
      .then((data) => {
        setOrdersData(data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [])

  return (
    <div>
      <CCard className="mb-4">
        <CCardHeader>
          <h3>Orders Table</h3>
        </CCardHeader>
        <CCardBody>
          <CTable striped>
            <CTableHead align="center">
              <CTableRow>
                <CTableHeaderCell scope="col">#</CTableHeaderCell>
                <CTableHeaderCell scope="col">Cost</CTableHeaderCell>
                <CTableHeaderCell scope="col">Note</CTableHeaderCell>
                <CTableHeaderCell scope="col">Order Date</CTableHeaderCell>
                <CTableHeaderCell scope="col">Shipped Date</CTableHeaderCell>
              </CTableRow>
            </CTableHead>
            <CTableBody>
              {
                OrdersData.map((ele, index) => {
                  return (
                    <CTableRow key={index}>
                      <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                      <CTableDataCell>{ele.cost.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}</CTableDataCell>
                      <CTableDataCell>{ele.note}</CTableDataCell>
                      <CTableDataCell>{ele.orderDate}</CTableDataCell>
                      <CTableDataCell>{ele.shippedDate}</CTableDataCell>
                    </CTableRow>
                  )
                })
              }
            </CTableBody>
          </CTable>
        </CCardBody>
      </CCard>
    </div>
  )
}

export default Orders
