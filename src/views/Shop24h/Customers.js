import React, { useEffect, useState } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from '@coreui/react'

const Customers = () => {
  const [CustomersData, setCustomersData] = useState([{
    address: "",
    city: "",
    country: "",
    email: "",
    fullName: "",
    phone: "",
  }])

  const fetchApi = async (url) => {
    const response = await fetch(url);
    const data = await response.json();
    return data;
  }

  useEffect(() => {
    fetchApi("http://localhost:8000/Customers")
      .then((data) => {
        setCustomersData(data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [])

  return (
    <div>
      <CCard className="mb-4">
        <CCardHeader>
          <h3>Customers Table</h3>
        </CCardHeader>
        <CCardBody>
          <CTable striped>
            <CTableHead align="center">
              <CTableRow>
                <CTableHeaderCell scope="col">#</CTableHeaderCell>
                <CTableHeaderCell scope="col">Full Name</CTableHeaderCell>
                <CTableHeaderCell scope="col">Email</CTableHeaderCell>
                <CTableHeaderCell scope="col">Phone</CTableHeaderCell>
                <CTableHeaderCell scope="col">Address</CTableHeaderCell>
              </CTableRow>
            </CTableHead>
            <CTableBody>
              {
                CustomersData.map((ele, index) => {
                  return (
                    <CTableRow key={index}>
                      <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                      <CTableDataCell>{ele.fullName}</CTableDataCell>
                      <CTableDataCell>{ele.email}</CTableDataCell>
                      <CTableDataCell>{ele.phone}</CTableDataCell>
                      <CTableDataCell>{ele.address}, {ele.city}, {ele.country}</CTableDataCell>
                    </CTableRow>
                  )
                })
              }
            </CTableBody>
          </CTable>
        </CCardBody>
      </CCard>
    </div>
  )
}

export default Customers
