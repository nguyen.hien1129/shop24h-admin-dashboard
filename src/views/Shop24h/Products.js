import React, { useEffect, useState } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from '@coreui/react'

const Products = () => {
  const [ProductsData, setProductsData] = useState([{
    buyPrice: 0,
    description: "",
    imageUrl: "",
    name: "",
    promotionPrice: 0,
    type: "",
    typeName: "",
  }])

  const fetchApi = async (url) => {
    const response = await fetch(url);
    const data = await response.json();
    return data;
  }

  useEffect(() => {
    fetchApi("http://localhost:8000/Products")
      .then((productsData) => {
        fetchApi("http://localhost:8000/ProductTypes/")
          .then((typeData) => {
            let arrayElement = {};
            let arrayResult = [];
            productsData.data.forEach(eleProducts => {
              typeData.data.forEach(eleType => {
                if (eleProducts.type === eleType._id) {
                  arrayElement = eleProducts;
                  arrayElement.typeName = eleType.name;
                  arrayResult.push(arrayElement);
                }
              });
            });
            setProductsData(arrayResult);
          })
          .catch((error) => {
            console.log(error);
          });
      })
      .catch((error) => {
        console.log(error);
      });
  }, [])

  return (
    <div>
      <CCard className="mb-4">
        <CCardHeader>
          <h3>Products Table</h3>
        </CCardHeader>
        <CCardBody>
          <CTable striped align="middle">
            <CTableHead>
              <CTableRow align="middle" className="text-center">
                <CTableHeaderCell scope="col">#</CTableHeaderCell>
                <CTableHeaderCell scope="col">Name</CTableHeaderCell>
                <CTableHeaderCell scope="col">Buy Price</CTableHeaderCell>
                <CTableHeaderCell scope="col">Promotion Price</CTableHeaderCell>
                <CTableHeaderCell scope="col">Description</CTableHeaderCell>
                <CTableHeaderCell scope="col">Categories</CTableHeaderCell>
              </CTableRow>
            </CTableHead>
            <CTableBody>
              {
                ProductsData.map((ele, index) => {
                  return (
                    <CTableRow key={index}>
                      <CTableHeaderCell scope="row" className="text-center">{index + 1}</CTableHeaderCell>
                      <CTableDataCell className="text-center">{ele.name}</CTableDataCell>
                      <CTableDataCell className="text-center">{ele.buyPrice.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}</CTableDataCell>
                      <CTableDataCell className="text-center">{ele.promotionPrice.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}</CTableDataCell>
                      <CTableDataCell>{ele.description}</CTableDataCell>
                      <CTableDataCell className="text-center">{ele.typeName}</CTableDataCell>
                    </CTableRow>
                  )
                })
              }
            </CTableBody>
          </CTable>
        </CCardBody>
      </CCard>
    </div>
  )
}

export default Products
