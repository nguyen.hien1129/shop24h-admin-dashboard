import React, { useEffect, useState } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from '@coreui/react'

const ProductTypes = () => {
  const [productTypesData, setProductTypesData] = useState([{
    description: "",
    name: ""
  }])

  const fetchApi = async (url) => {
    const response = await fetch(url);
    const data = await response.json();
    return data;
  }

  useEffect(() => {
    fetchApi("http://localhost:8000/ProductTypes")
      .then((typesData) => {
        setProductTypesData(typesData.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [])

  return (
    <div>
      <CCard className="mb-4">
        <CCardHeader>
          <h3>Product Types Table</h3>
        </CCardHeader>
        <CCardBody>
          <CTable striped>
            <CTableHead align="center">
              <CTableRow>
                <CTableHeaderCell scope="col">#</CTableHeaderCell>
                <CTableHeaderCell scope="col">Name</CTableHeaderCell>
                <CTableHeaderCell scope="col">Description</CTableHeaderCell>
              </CTableRow>
            </CTableHead>
            <CTableBody>
              {
                productTypesData.map((ele, index) => {
                  return (
                    <CTableRow key={index}>
                      <CTableHeaderCell scope="row">{index + 1}</CTableHeaderCell>
                      <CTableDataCell>{ele.name}</CTableDataCell>
                      <CTableDataCell>{ele.description}</CTableDataCell>
                    </CTableRow>
                  )
                })
              }
            </CTableBody>
          </CTable>
        </CCardBody>
      </CCard>
    </div>
  )
}

export default ProductTypes
